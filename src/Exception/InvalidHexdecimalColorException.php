<?php

namespace Drupal\hexidecimal_color\Exception;

use Exception;

/**
 * Exception shown when an invalid value is passed for a hexidecimal color.
 */
class InvalidHexdecimalColorException extends Exception() {}
